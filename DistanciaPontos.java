import java.util.Scanner;
import java.lang.Math;

public class DistanciaPontos{
	public static void main (String args[]){
				Scanner sc = new Scanner (System.in);
				
		System.out.println("Digite o X do primeiro ponto");
		int xp1 = sc.nextInt();
		System.out.println("Digite o Y do primeiro ponto");
		int yp1 = sc.nextInt();
		
		System.out.println("Digite o X do segundo ponto");
		int xp2 = sc.nextInt();
		System.out.println("Digite o Y do segundo ponto");
		int yp2 = sc.nextInt();
		
		int mediax = xp1-xp2;
		int mediay = yp1-yp2;
		
		double	distancia = Math.sqrt ( Math.pow(mediax,2) + Math.pow(mediay,2) );
		System.out.printf ("A distância desses pontos é" + distancia);
		
	
		
	}
}	